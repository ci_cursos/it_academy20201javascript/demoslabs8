import express from 'express';
import bodyParser from 'body-parser';

const app = express();
const port = 3000;

app.use(consoleLogger);
app.use(bodyParser.json());

app.get('/', (req, res) => {
    const metodo = req.method;
    res.send(`Alô ${metodo}`);
});

app.get('/alo/:nome', (req, res) => {
    const nome = req.params.nome;
    res.send(`Alô ${nome}`);
});

app.post('/alo', (req, res) => {
    const {nome} = req.body;
    if (nome) {
        res.send(`Alô ${nome}`);
    } else {
        res.status(400).send('Objeto inválido');
    }
});

const server = app.listen(port, () => {
    console.log(`Express na porta ${port}`);
});

function consoleLogger(req: express.Request, res: express.Response, next: express.NextFunction) {
    console.log(`${req.method} ${req.path}`);
    next();
}